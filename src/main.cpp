// other files
#include "file_list.h"
#include "log.h"

//stdlibs
#include <SDL2/SDL_keyboard.h>
#include <SDL2/SDL_keycode.h>
#include <unistd.h>

// libraries
// sdl
#include <SDL.h>

// stb
#define STB_RECT_PACK_IMPLEMENTATION
#define STB_TRUETYPE_IMPLEMENTATION
#define STBTTF_IMPLEMENTATION
#include "stbttf.h"

void draw_header(STBTTF_Font* cmd_font);
void draw_command_bar(STBTTF_Font* cmd_font);
void draw_background();
void draw_files(STBTTF_Font* files_font);

char command_str[512] = {0};
uint16_t command_str_len = 0;
int16_t selected_index = 0;
char selected_file[256];
SDL_Renderer* renderer;
SDL_Window *window;

// @TODO(Renzix): put somewhere better?
bool show_hidden = true;

bool has_to_render = true;

int main(int argc, char *argv[]) {
    // now we init all the sdl stuff

    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow(
        "An SDL2 window",                  // window title
        SDL_WINDOWPOS_UNDEFINED,           // initial x position
        SDL_WINDOWPOS_UNDEFINED,           // initial y position
        640,                               // width, in pixels
        480,                               // height, in pixels
        SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE // flags - see below
    );
    if (window == NULL) {
        log(ERROR, "Could not create window: %s\n", SDL_GetError());
        return 1;
    }
    renderer = SDL_CreateRenderer(window, -1, 0);
    draw_background();
    SDL_RenderPresent(renderer);

    STBTTF_Font* cmd_font = STBTTF_OpenFont(renderer, "/usr/share/fonts/dejavu/DejaVuSansMono.ttf", 24);
    STBTTF_Font* files_font = STBTTF_OpenFont(renderer, "/usr/share/fonts/dejavu/DejaVuSansMono.ttf", 16);

    // The window is open: could enter program loop here (see SDL_PollEvent())
    SDL_Event ev;
    bool running = true;
    while (running) {
        SDL_PollEvent(&ev);
        switch (ev.type) {
            case SDL_WINDOWEVENT: {
                // @TODO(Renzix): actually differentiate between window events
                // to see if we need to redraw
                draw_background();
                draw_header(cmd_font);
                draw_files(files_font);
                draw_command_bar(cmd_font);
                break;
            }
            case SDL_QUIT: {
                running = false;
                break;
            }
            case SDL_KEYDOWN: {
                switch (ev.key.keysym.sym) {
                    case SDLK_ESCAPE: {
                        break;           // @TODO(Renzix): make this "search mode"
                    }
                    // @TODO(Renzix): c is a bad language and i don't like
                    // extensions, textinput events might fix this
                    case SDLK_a:
                    case SDLK_b:
                    case SDLK_c:
                    case SDLK_d:
                    case SDLK_e:
                    case SDLK_f:
                    case SDLK_g:
                    case SDLK_h:
                    case SDLK_i:
                    case SDLK_j:
                    case SDLK_k:
                    case SDLK_l:
                    case SDLK_m:
                    case SDLK_n:
                    case SDLK_o:
                    case SDLK_p:
                    case SDLK_q:
                    case SDLK_r:
                    case SDLK_s:
                    case SDLK_t:
                    case SDLK_u:
                    case SDLK_v:
                    case SDLK_w:
                    case SDLK_x:
                    case SDLK_y:
                    case SDLK_z:
                    case SDLK_1:
                    case SDLK_2:
                    case SDLK_3:
                    case SDLK_4:
                    case SDLK_5:
                    case SDLK_6:
                    case SDLK_7:
                    case SDLK_8:
                    case SDLK_9:
                    case SDLK_0:
                    case SDLK_SPACE:
                    case SDLK_EXCLAIM:
                    case SDLK_PERIOD:
                    case SDLK_COMMA:
                    case SDLK_SLASH:
                    case SDLK_BACKSLASH:
                    case SDLK_DOLLAR:
                    case SDLK_MINUS: {
                        log(VERBOSE_DEBUG, "LETTER: %s", SDL_GetKeyName(ev.key.keysym.sym));
                        log(VERBOSE_DEBUG, "LETTER: %s", SDL_GetScancodeName(ev.key.keysym.scancode));
                        if (ev.key.keysym.mod & KMOD_CTRL) {
                            switch(ev.key.keysym.sym) {
                                case 'q':  {
                                    running=false;
                                    break;
                                }
                                case 'h': {
                                    show_hidden = !show_hidden;
                                    break;
                                }
                                default: {
                                    log(DEBUG, "control + %c", ev.key.keysym.sym);
                                    break;
                                }
                            }
                        } else if (ev.key.keysym.mod & KMOD_SHIFT) {
                            if (isalpha(ev.key.keysym.sym))
                                command_str[command_str_len++] = toupper(ev.key.keysym.sym);
                            else {
                                char ch;
                                //  @TODO(Renzix): look into textinput events
                                //  instead so I dont have to do this bull
                                switch (ev.key.keysym.sym) {
                                    case '1': {
                                        ch = '!';
                                        break;
                                    }
                                    case '2': {
                                        ch = '@';
                                    }
                                    case '3': {
                                        ch = '#';
                                        break;
                                    }
                                    case '4': {
                                        ch = '$';
                                        break;
                                    }
                                    case '5': {
                                        ch = '%';
                                        break;
                                    }
                                    case '6': {
                                        ch = '^';
                                        break;
                                    }
                                    case '7': {
                                        ch = '&';
                                        break;
                                    }
                                    case '8': {
                                        ch = '*';
                                        break;
                                    }
                                    case '9': {
                                        ch = '(';
                                        break;
                                    }
                                    case '0': {
                                        ch = ')';
                                        break;
                                    }
                                    case ',': {
                                        ch = '<';
                                        break;
                                    }
                                    case '.': {
                                        ch = '>';
                                        break;
                                    }
                                    case '/': {
                                        ch = '?';
                                        break;
                                    }
                                    case '\\': {
                                        ch = '|';
                                        break;
                                    }
                                    default: {
                                        log(WARNING, "Unknown keysym with shift: %c", ev.key.keysym.sym);
                                        ch = ' ';
                                        break;
                                    }
                                }
                                command_str[command_str_len++] = ch;
                            }
                        } else {
                            command_str[command_str_len++] = ev.key.keysym.sym;
                        }
                        // @TODO(Renzix): control/alt letters
                        draw_background();
                        draw_header(cmd_font);
                        draw_files(files_font);
                        draw_command_bar(cmd_font);
                        break;
                    }
                    case SDLK_UP: {
                        selected_index -= 1;
                        draw_background();
                        draw_header(cmd_font);
                        draw_files(files_font);
                        draw_command_bar(cmd_font);
                        break;
                    }
                    case SDLK_DOWN: {
                        selected_index += 1;
                        draw_background();
                        draw_header(cmd_font);
                        draw_files(files_font);
                        draw_command_bar(cmd_font);
                        break;
                    }
                    case SDLK_BACKSPACE: {
                        // @TODO(Renzix): control backspace
                        if (command_str_len > 0) {
                            command_str[--command_str_len] = '\0';
                        }
                        draw_background();
                        draw_header(cmd_font);
                        draw_files(files_font);
                        draw_command_bar(cmd_font);
                        break;
                    }
                    case SDLK_DELETE: {
                        chdir("..");
                        memset(command_str, '\0', command_str_len);
                        command_str_len = 0;
                        draw_background();
                        draw_header(cmd_font);
                        draw_files(files_font);
                        draw_command_bar(cmd_font);
                        break;
                    }
                    case SDLK_RETURN: {
                        // @TODO(Renzix): parse command_str
                        if (command_str[0] == '$') {
                            // @TODO(Renzix): consider using another exec thingy and getting command output?
                            //
                            pid_t pid = 0;
                            if ((pid = fork()) == 0) {
                                // @TODO(DeBruno): parse args?
                                char* tok = strtok(command_str+1, " ");
                                char *args[100] = {0}; //= {tok, "", NULL};
                                args[0] = tok;
                                // get rest and put into *args
                                for (int i = 1; tok!=NULL; i++) {
                                    tok = strtok(NULL, " ");
                                    args[i] = tok;
                                }
                                log(DEBUG, "EXECUTING SHELL: %s", args[0]);
                                execvp(args[0], args);
                                exit(0);
                            }
                            memset(command_str, '\0', command_str_len);
                            command_str_len = 0;
                        } else {
                            chdir(selected_file);
                            memset(command_str, '\0', command_str_len);
                            command_str_len = 0;
                        }
                        draw_background();
                        draw_header(cmd_font);
                        draw_files(files_font);
                        draw_command_bar(cmd_font);
                        // @TODO(Renzix): if alt then enter command
                        break;
                    }
                    default: {
                        log(DEBUG, "Unbound Scancode: %s", SDL_GetScancodeName(ev.key.keysym.scancode));
                        break;
                    }
                }
                break;
            }
            default: {
                log(VERBOSE_DEBUG, "unknown event");
                break;
            }
        }
        if (has_to_render) {
            SDL_RenderPresent(renderer);
            has_to_render = false;
        } else {
            SDL_Delay(5);
        }
    }

    log(INFO, "ENDING, deleting all open resources");
    STBTTF_CloseFont(cmd_font);
    STBTTF_CloseFont(files_font);
    // Close and destroy the window
    SDL_DestroyWindow(window);

    // Clean up
    SDL_Quit();

    return 0;
}

// @TODO(Renzix): dont draw over header/command_bar
void draw_background() {
    SDL_SetRenderDrawColor(renderer, 27, 1, 31, 255);
    SDL_RenderClear(renderer);
    has_to_render = true;
}

void draw_header(STBTTF_Font* cmd_font) {
    //create surface
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    SDL_Surface* header_surface = SDL_CreateRGBSurface(0,w,h,32,0,0,0,0);
    // get texture
    SDL_Texture* header_texture = SDL_CreateTextureFromSurface(renderer, header_surface);
    // render it
    SDL_Rect rect = {0, 0, w, 20}; //create a rect  @TODO(Renzix): font size based height?
    SDL_RenderCopy(renderer, header_texture, NULL, &rect);
    SDL_FreeSurface(header_surface);

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    STBTTF_RenderText(renderer, cmd_font, 0, 20, get_current_dir_name(), false);
    has_to_render = true;
}

void draw_command_bar(STBTTF_Font* cmd_font) {
    //create surface
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    SDL_Surface* bar_surface = SDL_CreateRGBSurface(0,w,h,32,0,0,0,0);
    // get texture
    SDL_Texture* bar_texture = SDL_CreateTextureFromSurface(renderer, bar_surface);
    if (bar_texture == NULL){
        log(ERROR, "Couldn't create bar texture");
        return;
    }
    SDL_Rect bar_rect = {0, h-32, w, h}; //create a rect @TODO(Renzix): font size?
    SDL_RenderCopy(renderer, bar_texture, NULL, &bar_rect);
    SDL_FreeSurface(bar_surface);

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    STBTTF_RenderText(renderer, cmd_font, 1, h-8, command_str, false);
    has_to_render = true;
}

// @TODO(Renzix): make this 2 panes?
void draw_files(STBTTF_Font* files_font) {
    char buff[5000] = {0};
    char* buffer = buff;
    refresh_files(get_current_dir_name());
    int num_of_files = search_file_list(&buffer, show_hidden, command_str);
    if (selected_index >= num_of_files)
        selected_index = 0;
    else if (selected_index < 0)
        selected_index = num_of_files-1;
    char* tok = strtok(buffer, "\n");
    int y = 40;
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    int i;
    for (i = 0; tok!=NULL; i++) {
        STBTTF_RenderText(renderer, files_font, 1, y, tok, i==(selected_index));
        log(VERBOSE_DEBUG, "tok: %s", tok);
        if (i == selected_index)
            strcpy(selected_file, tok);
        tok = strtok(NULL, "\n");
        y+=18; // @TODO(Renzix): actually find the line height?
    }
    has_to_render = true;
}
