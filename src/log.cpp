#include "log.h"

// @TODO(Renzix): make good
void log(LOG_TYPE visibility, char* format, ...) {
    if (CURRENT_VISIBILITY > visibility) {
        return;
    }
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    puts("");
    va_end(args);
}
