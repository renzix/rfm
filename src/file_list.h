#ifndef FILE_LIST_H
#define FILE_LIST_H

#include <dirent.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#include "log.h"

struct file_info {
    char d_name[256];
    unsigned char d_type;
    bool hidden;
};

#define MAX_FILE_LIST 10000 // @TODO(Renzix): maybe save this in a map
static file_info file_list[MAX_FILE_LIST] = {0}; // @TODO(Renzix): make this dynamic
static uint64_t file_list_len = 0;

int refresh_files(const char* path);
void print_files(void);
int search_file_list(char** buffer, bool show_hidden, char* pattern);
bool search_contains(char* word, char* pattern);

#endif
