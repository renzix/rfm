#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <stdarg.h>

enum LOG_TYPE {
  VERBOSE_DEBUG = 0,
  DEBUG = 1,
  INFO = 2,
  WARNING = 3,
  ERROR = 4
};

static LOG_TYPE CURRENT_VISIBILITY = DEBUG;

void log(LOG_TYPE visibility, char* format, ...);
#endif
