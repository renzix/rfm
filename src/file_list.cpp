#include "file_list.h"

int refresh_files(const char* path) {
    for (uint64_t i = 0; i < file_list_len; i++) {
        memset(&file_list[i], 0, sizeof(file_info));
        file_list_len = 0;
    }
    DIR* d;
    struct dirent* dir;
    d = opendir(path);
    if (!d)
        return -1;
    while ((dir = readdir(d)) != NULL) {
        file_info temp;
        // get name
        strncpy(temp.d_name, dir->d_name, 256);
        // get type
        temp.d_type = dir->d_type;
        // check if hidden
        if ((temp.d_name[0] == '.') && (strcmp("..", dir->d_name) != 0) && (strcmp(".", dir->d_name) != 0))
            temp.hidden = true;
        else
            temp.hidden = false;
        // @TODO(Renzix): directories
        file_list[file_list_len++] = temp;
    }
    return file_list_len;
}

void print_files(void) {
    for (uint64_t i = 0; i < file_list_len; i++) {
        log(DEBUG, "File: %s", file_list[i].d_name);
    }
}

int search_file_list(char** buffer, bool show_hidden, char* pattern) {
    uint64_t current_buff_loc = 0;
    int new_file_list_length = 0;
    for (uint64_t i = 0; i < file_list_len; i++) {
        log(VERBOSE_DEBUG, "strstr: %s %i, %i", file_list[i].d_name ,search_contains(file_list[i].d_name, pattern), pattern[0]!='\0');
        if (!(search_contains(file_list[i].d_name, pattern))&&(pattern[0]!='\0'))
            continue;
        if ((file_list[i].hidden == true) && (show_hidden == false))
            continue;
        char temp_str[257];
        snprintf(temp_str, (strlen(file_list[i].d_name)+2),"%s\n", file_list[i].d_name);
        strncpy((*buffer)+current_buff_loc, temp_str, strlen(temp_str));
        current_buff_loc+=strlen(temp_str);
        new_file_list_length++;
    }
    file_list_len = new_file_list_length;
    log(VERBOSE_DEBUG, "Buffer: %s", *buffer);
    return file_list_len;
}

bool search_contains(char* word, char* pattern) {
    int new_normal = 0;
    bool letter_found = false;
    int j = 0;
    for(int i = 0; i < strlen(pattern); i++) {
        letter_found = false;
        for (;j < strlen(word); j++) {
            if (pattern[i] == word[j]) {
                letter_found = true;
                j++;
                break;
            }
        }
        log(VERBOSE_DEBUG, "letter found %c found is %i", pattern[i], letter_found);
        if (letter_found != true) {
            return false;
        }
    }
    return true;
}
