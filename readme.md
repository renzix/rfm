# RFM

This is meant to be the next generation of file managers and take interesting
ideas and concepts from emacs (ivy/helm, magit) and concepts from other file
managers (mainly twin pane ones like midnight commander and krusader).

This is not meant to compete with normal file managers. This is meant to compete
with zsh and fish.

## Install

For now we use a simple one line to build everything. That stay for the
foreseeable future due to me disliking build systems.

## Keybinds/Usage

This usage may or may not change in the future and is how I currently navigate.

### Examples

This compares POSIX shell to what you would do in rfm. Note this does not
include aliases, zsh unsensitive case and some shells like zsh supporting just typing the name of a
directory and not using cd.

cd /home/genzix/Local/rfm/src/ then open main.cpp

in posix shell
```
cd Loc<TAB>rfm<TAB>src<RET>
vim main.cpp<RET>
```

in rfm
```
loc<RET>rfm<RET>src<RET>mai<RET>
```

Download Proton GE and install it (ignore the fact that proton GE has commands
on his github which you can copy/paste)

in posix shell
```sh
wget https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton7-24/GE-Proton7-24.tar.gz #copy/paste
mkdir ~/.ste<TAB>root<TAB>compatibilitytools.d
tar -xf GE-<TAB> -C ~/.ste<TAB>root<TAB>compat<TAB>

# OR

wget https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton7-24/GE-Proton7-24.tar.gz #copy/paste
mkdir ~/.ste<TAB>root<TAB>compatibilitytools.d<RET>
cd $_<RET>
tar -xf ~/Down<TAB>GE-<TAB> -C .<RET>
```

in rfm
```
$wget https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton7-24/GE-Proton7-24.tar.gz # copy/paste
.ste<RET>root<RET>compatibilitytools.d<RET>
$tar -xf ~/Down<TAB>GE-<TAB> -C .<RET>
```

Watch videos

in posix shell
```sh
cd Loc<TAB>hen<TAB><RET>
mpv boku-n<TAB>5.<TAB>
```

in rfm
```
loc<RET>hen<RET>boku 5.<RET>
```

## Why

### Search is Priority

At the bottom of your screen you have something called the command bar. This is
how you interact with the file manager. The command bar has 2 different modes
(for now, may add more in future). The first mode is the search mode which
functionally acts like a easier to use terminal for cd. Note when you switch
between them, the filter does not go away (important for later).

### Shell

The second mode is the shell mode which gets parsed by us in our own shell (This
is so cool but i will probably never finish it). The shell mode is POSIX (i
hope) and contains variables for all the files, currently selected files and a
builtin command to filter and return specific files. The reason why it is POSIX
is simply because copy/paste is extremely important.

## Future

There are SOOOOOOOOOOOOOOOOO many things I can add or ideas to implement. Here
are a few.

### Twin panes

This feature would make it way easier to see 2 different folders at once and
move stuff in-between which is more of a strength of gui file managers then the
command line. For now I want to get users from the command line. It would also
cause me to rewrite a lot of stuff but I want to get something working first.

### Built-in Editor

This would be difficult but ultimately I think it could a amazing addition.
Would also open the door for other things and give you another reason to use the
file manager.

### Magit Clone

Magit is amazing and it would be a amazing addition to what I have here already.
This would be even more difficult and would require me to rework the ui.

### Project manager

I could totally see another mode called "project" so you don't have to worry
about your current build system and can use `build` to build and `run` to run
(among other stuff like searching the project with grep and find). 
